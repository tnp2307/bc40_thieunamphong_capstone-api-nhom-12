export class CartItem {
  constructor(_product, _price, _quantity) {
    this.product = _product;
    this.price = _price;
    this.quantity = _quantity;
    this.totalPrice =function(){return (this.price*this.quantity)} 
  }
}
