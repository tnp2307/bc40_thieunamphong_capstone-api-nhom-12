import {
  createFilterItemArr,
  isItemInCart,
  renderCartList,
  renderPhoneList,
  tinhTotalCartPrice,
} from "./controller.js";
import { CartItem } from "./model.js";
const BASE_URL = "https://63df7e58a76cfd410583550a.mockapi.io";
let shoppingCart = [];
let SHOPPINGCART = "SHOPPINGCART";
let shoppingCartJson = localStorage.getItem(SHOPPINGCART);
if (shoppingCartJson != null) {
  let cartArr = JSON.parse(shoppingCartJson);
  shoppingCart = cartArr.map(function (item) {
    return new CartItem(item.product, item.price, item.quantity);
  });
}
renderCartList(shoppingCart);
tinhTotalCartPrice(shoppingCart);

function fetchPhoneList() {
  axios({
    url: `${BASE_URL}/phoneStore`,
    method: "GET",
  })
    .then(function (res) {
      // xử lý khi gọi API thành công
      renderPhoneList(res.data);
    })
    .catch(function (err) {
      // xử lý khi gọi API thất bại
      console.log(err);
    });
}
fetchPhoneList();
let filterPhone = () => {
  let filterValue = document.getElementById("txt-filter").value;
  axios({
    url: `${BASE_URL}/phoneStore`,
    method: "GET",
  })
    .then(function (res) {
      // xử lý khi gọi API thành công
      let filterArr = createFilterItemArr(res.data, filterValue);
      renderPhoneList(filterArr);
    })
    .catch(function (err) {
      // xử lý khi gọi API thất bại
      console.log(err);
    });
};

let themVaoGio = (id) => {
  axios({
    url: `${BASE_URL}/phoneStore`,
    method: "GET",
  })
    .then(function (res) {
      // xử lý khi gọi API thành công
      let phoneArr = [];
      phoneArr = res.data;
      let viTri = phoneArr.findIndex((item) => item.id == id);

      if (shoppingCart.length == 0) {
        let buyItem = new CartItem(phoneArr[viTri], phoneArr[viTri].price, 1);
        shoppingCart.push(buyItem);
      } else {
        // let loopLenght = shoppingCart.length;
        if (isItemInCart(shoppingCart, phoneArr[viTri].id) != -1) {
          let viTriCart = shoppingCart.findIndex(
            (item) => item.product.id == id
          );
          shoppingCart[viTriCart].quantity++;
        } else {
          let buyItem = new CartItem(phoneArr[viTri], phoneArr[viTri].price, 1);
          shoppingCart.push(buyItem);
        }
      }
      tinhTotalCartPrice(shoppingCart);
      let shoppingCartJson = JSON.stringify(shoppingCart);
      localStorage.setItem(SHOPPINGCART, shoppingCartJson);
      renderCartList(shoppingCart);
    })
    .catch(function (err) {
      // xử lý khi gọi API thất bại
      console.log(err);
    });
};
let xoaPhone = (id) => {
  let viTriCart = shoppingCart.findIndex((item) => item.product.id == id);
  if (shoppingCart[viTriCart].quantity > 1) {
    shoppingCart[viTriCart].quantity--;
  } else {
    shoppingCart.splice(viTriCart, 1);
  }
  tinhTotalCartPrice(shoppingCart);
  let shoppingCartJson = JSON.stringify(shoppingCart);
  localStorage.setItem(SHOPPINGCART, shoppingCartJson);
  renderCartList(shoppingCart);
};

let thanhToan = () => {
  shoppingCart = [];
  tinhTotalCartPrice(shoppingCart);
  let shoppingCartJson = JSON.stringify(shoppingCart);
  localStorage.setItem(SHOPPINGCART, shoppingCartJson);
  renderCartList(shoppingCart);
};

window.filterPhone = filterPhone;
window.themVaoGio = themVaoGio;
window.isItemInCart = isItemInCart;
window.xoaPhone = xoaPhone;
window.thanhToan = thanhToan;
