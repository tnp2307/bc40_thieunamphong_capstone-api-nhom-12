export let renderPhoneList = (phoneList) => {
  let contentHTML = "";
  phoneList.forEach((item) => {
    var contentDiv = /*html*/ `
        <div class="container col-3 p-3 my-3">
          <div class="phoneItem  ">
          <div class="d-flex justify-content-center"><img width ="150px" height ="150px" src="${item.img}" alt="" /></div>
               <div id="name" >${item.name}</div>
               <div id="price" > ${item.price}&nbsp;$</div>
               <div id="description">Description ${item.desc}</div>
               <div id="screen" >Screen: ${item.screen}</div>
               <div id="back-camera" >Back Camera: ${item.backCamera}</div>
               <div id=" front-camera">Front Camera: ${item.frontCamera}</div>
               <button class="btn btn-success mt-2" onclick="themVaoGio('${item.id}')" >Thêm vào giỏ hàng</button>
               <p class="product__price--show">
          </div>
        </div> `;
    contentHTML += contentDiv;
  });
  document.getElementById("phoneList").innerHTML = contentHTML;
};
export let createFilterItemArr = (phoneList, filterValue) => {
  let filterArr = [];
  phoneList.forEach((item) => {
    console.log(item.type);
    if (item.type == filterValue) {
      filterArr.push(item);
    }
  });
  console.log(filterArr);
  return filterArr;
};
export let isItemInCart = (shoppingCart, id) => {
  let viTri = -1;
  for (let index = 0; index < shoppingCart.length; index++) {
    if (shoppingCart[index].product.id == id) {
      return (viTri = index);
    }
  }
  return (viTri = -1);
};

export let renderCartList = (cartArr) => {
  let contentHTML = "";
  cartArr.forEach((item) => {
    var contentTr = /*html*/ `
        <tr>
        <td><img class="" width ="80px" height ="80px" src="${
          item.product.img
        }"  /></td>
        <td id="name">   ${item.product.name}</td>
        <td id="price"> ${item.totalPrice()}&nbsp;$</td>
        <td>
        <button class="btn btn-danger" onclick="xoaPhone('${
          item.product.id
        }')" >-</button>
        <div class="qty btn btn-outline-dark inline" id="price" > ${
          item.quantity
        }</div>
        <button class="btn btn-success" onclick="themVaoGio('${
          item.product.id
        }')" >+</button>
        </td>
        </tr>
           
        `;
    contentHTML += contentTr;
  });
  console.log(contentHTML);
  document.getElementById("tbodycartList").innerHTML = contentHTML;
};
export let tinhTotalCartPrice = (cartArr) => {
  let total = 0;
  cartArr.forEach((item) => {
    let totalPriceItem = item.totalPrice();
    total += totalPriceItem;
  });
  if (total == 0) {
    document.getElementById(
      "totalPrice"
    ).innerHTML = `Bạn chưa có món nào trong giỏ hàng`;
    document.getElementById("totalPrice").classList.add("text-danger")
  } else {
    document.getElementById(
      "totalPrice"
    ).innerHTML = `${total}&nbsp;$`;
    document.getElementById("totalPrice").classList.remove("text-danger")
  }
};
